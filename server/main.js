const express = require('express')
const app = express()
const http = require('http')
const server = http.createServer(app)
const io = require('socket.io')(server, {
  cors: {
    origin: '*',
    methods: ['GET', 'POST'],
  },
})

io.on('connection', (client) => {
  io.emit('connectedClients', io.engine.clientsCount)

  client.on('headerTopicChange', (data) => {
    io.emit('headerTopicChange', data)
  })

  client.on('votingItemSubmitted', (data) => {
    io.emit('votingItemSubmitted', data)
  })

  client.on('toggleVote', (data) => {
    let vote = {
      user: client.id,
      item: data,
    }
    io.emit('toggleVote', vote)
  })

  client.on('removeTopic', (data) => {
    io.emit('removeTopic', data)
  })

  client.on('disconnect', () => {
    io.emit('connectedClients', io.engine.clientsCount)
  })
})

app.get('/', (req, res) => {
  res.send('Hello World!')
})

const port = process.env.PORT || 3000

server.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
