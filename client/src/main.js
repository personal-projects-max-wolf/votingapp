import Vue from 'vue'
import Vuex from 'vuex'
import { store } from './store/store'
import App from './App.vue'
import { vueSocketIO } from './socket/socket'

Vue.use(Vuex)
Vue.use(vueSocketIO)

new Vue({
  store,
  render: h => h(App),
}).$mount('#app')
