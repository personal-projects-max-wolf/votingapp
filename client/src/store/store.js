import Vue from 'vue'
import Vuex from 'vuex'
import Color from 'color'

Vue.use(Vuex)

const state = {
  headerTopic: '',
  activeUsers: 1,
  topicId: 0,
  topics: [
    // {
    //   id: 1,
    //   title: 'Lorem Ipsum',
    //   voted: [1, 2],
    //   color: 'red',
    // },
  ],
}

const mutations = {
  updateTopic(state, value) {
    state.headerTopic = value
  },
  SOCKET_headerTopicChange(state, value) {
    state.headerTopic = value
  },
  SOCKET_connectedClients(state, value) {
    state.activeUsers = value
  },
  SOCKET_votingItemSubmitted(state, value) {
    ++state.topicId
    let newTopic = {
      id: state.topicId,
      title: value,
      voted: [],
      color: 'red',
    }

    state.topics.push(newTopic)
    store.commit('updateTopicColors')
  },
  SOCKET_removeTopic(state, value) {
    state.topics = state.topics.filter(x => x.id != value)
  },
  SOCKET_toggleVote(state, value) {
    let index = state.topics.findIndex(x => x.id == value.item)

    if (!state['topics'][index]['voted'].includes(value.user)) {
      state['topics'][index]['voted'].push(value.user)
    } else {
      state['topics'][index]['voted'] = state['topics'][index]['voted'].filter(
        voted => voted != value.user
      )
    }

    store.commit('updateTopicColors')
  },
  updateTopicColors(state) {
    const black = '#030714'
    const blue = '#274DE5'
    const votes = state.topics.map(topic => topic.voted.length)
    const votesMax = Math.max(...votes)

    state.topics.forEach(topic => {
      let percentage = topic.voted.length / votesMax
      percentage = isNaN(percentage) ? 0 : percentage
      const color = Color(black).mix(Color(blue), percentage)
      topic.color = color
    })
  },
}

const getters = {
  headerTopic: state => state.headerTopic,
  activeUsers: state => state.activeUsers,
  topics: state => state.topics,
}

export const store = new Vuex.Store({
  state: state,
  mutations: mutations,
  getters: getters,
})
