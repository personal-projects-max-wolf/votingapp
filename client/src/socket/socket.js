import VueSocketIO from 'vue-socket.io'
import SocketIO from 'socket.io-client'
import { store } from '../store/store'

const socketURL =
  process.env.NODE_ENV === 'development'
    ? 'http://localhost:3000'
    : 'https://votingapp-se.herokuapp.com'
const socketConnection = SocketIO(socketURL)

export const vueSocketIO = new VueSocketIO({
  debug: false,
  connection: socketConnection,
  vuex: {
    store,
    actionPrefix: 'SOCKET_',
    mutationPrefix: 'SOCKET_',
  },
})
